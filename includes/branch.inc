<?php

/**
 * Contains the "Branch API" classes.
 */

/**
 * Implements a stack of branches to allow for nested default contexts during a
 * single request processing.
 */
class BranchStack {

  /**
   * A singleton instance of the branch stack.
   *
   * @var BranchStack
   */
  protected static $instance;

  /**
   * Returns the singleton branch stack.
   *
   * @return BranchStack
   *   A branch stack instance.
   */
  public static function get() {
    if (!isset(static::$instance)) {
      static::$instance = new static();
      $configuration = variable_get('branch_configuration', array());
      static::$instance->setConfiguration($configuration);
    }
    return static::$instance;
  }

  /**
   * The current configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The actual branch stack.
   *
   * @var \SplStack
   */
  protected $branches;

  /**
   * Constructs a new BranchStack instance.
   */
  public function __construct() {
    $this->branches = new \SplStack();
  }

  /**
   * Sets the current configuration.
   *
   * @param array $configuration
   *   The current configuration.
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * Pushes a new default context on the stack.
   *
   * @param array $context
   *   An associative array of context items keyed by axis name.
   */
  public function pushContext(array $context) {
    $branch = new Branch($this->configuration, $context);
    $this->branches->push($branch);
  }

  /**
   * Removes the current default context.
   *
   * @return array
   *   The previous default context.
   */
  public function popContext() {
    $branch = $this->branches->pop();
    return $branch ? $branch->getContext() : NULL;
  }

  /**
   * Retrieves the current branch.
   *
   * This method actually serves as a factory for Branch objects.
   *
   * @return Branch
   *   The branch object containing the current default context.
   */
  public function getCurrentBranch() {
    if ($this->branches->isEmpty()) {
      $this->branches->push(new Branch($this->configuration));
    }
    return $this->branches->top();
  }

}

/**
 * Implements the branching logic for various use cases:
 * - code execution;
 * - access checking;
 * - variable values retrieving;
 * - theme template preprocessing.
 *
 * Each branch contains a default context, holding default values for context
 * variation axes.
 */
class Branch {

  /**
   * Use this constant to refer to the base item/callback in configuration
   * entries.
   *
   * @var int
   */
  const BASE_REFERENCE = -1;

  /**
   * The current configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The current default context.
   *
   * @var array
   */
  protected $context;

  /**
   * The last selected context key.
   *
   * @var string[]
   */
  protected $lastContextKey;

  /**
   * Constructs a new Branch instance.
   *
   * @param array $configuration
   *   The current configuration.
   * @param array $context
   *   (optional) The branch default context. Defaults to an empty context.
   */
  public function __construct(array $configuration, array $context = array()) {
    $this->configuration = $configuration;
    $this->context = $context;
  }

  /**
   * Retrieves the current branch from the branch stack.
   *
   * @return Branch
   *   The current branch object.
   */
  public static function get() {
    return BranchStack::get()->getCurrentBranch();
  }

  /**
   * Returns the current default context.
   *
   * @return array
   *   An associative array of context items keyed by axis name.
   *
   * @see BranchStack::pushContext()
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Returns the context derived by parsing the specified context key.
   *
   * @param string $key
   *   The context key.
   * @param bool $merge
   *   (optional) Whether the returned context should be merged with default
   *   one. Defaults to FALSE.
   *
   * @return mixed[]
   *   The context array.
   */
  public function getContextFromKey($key, $merge = FALSE) {
    $context = $merge ? $this->context : array();
    foreach (explode('&', $key) as $axis_value) {
      list($axis, $value) = explode('=', $axis_value);
      if (is_numeric($value)) {
        $value = intval($value) == $value ? intval($value) : doubleval($value);
      }
      $context[$axis] = $value;
    }
    return $context;
  }

  /**
   * Returns the last selected context key.
   *
   * @return string
   *   A key identifying the last selected context.
   */
  public function getLastContextKey() {
    return $this->lastContextKey;
  }

  /**
   * Checks whether the user has access to the specified operation.
   *
   * @param string $operation
   *   The operation name. If a configuration item can be found, its value will
   *   be returned. Otherwise, if a callable is found matching the operation it
   *   will be called and its result will be returned. Any additional parameter,
   *   for instance the account object, will be forwarded to the callable.
   *
   * @return bool
   *   TRUE if the user has access to the operation, FALSE otherwise.
   *
   * @throws \LogicException
   *   When no matching item can be found.
   */
  public function access($operation) {
    $args = func_get_args();
    array_splice($args, 1, 0, array(array()));
    return (bool) call_user_func_array(array($this, 'executeWithContext'), $args);
  }

  /**
   * Checks whether the user has access to the specified operation.
   *
   * @param string $operation
   *   The operation name. If a configuration item can be found, its value will
   *   be returned. Otherwise, if a callable is found matching the operation it
   *   will be called and its result will be returned. Any additional parameter,
   *   for instance the account object, will be forwarded to the callable.
   * @param array $context
   *   A specific context to be merged with the default one. Axis values
   *   specified here take precedence over the default ones.
   *
   * @return bool
   *   TRUE if the user has access to the operation, FALSE otherwise.
   *
   * @throws \LogicException
   *   When no matching item can be found.
   */
  public function accessWithContext($operation, array $context) {
    return (bool) call_user_func_array(array($this, 'executeWithContext'), func_get_args());
  }

  /**
   * Executes the specified operation.
   *
   * @param string $operation
   *   The operation name. If a configuration item can be found, its value will
   *   be returned. Otherwise, if a callable is found matching the operation it
   *   will be called and its result will be returned. Any additional parameter
   *   will be forwarded to the callable.
   *
   * @return mixed
   *   The operation result.
   *
   * @throws \LogicException
   *   When no matching item can be found.
   */
  public function execute($operation) {
    $args = func_get_args();
    array_splice($args, 1, 0, array(array()));
    return call_user_func_array(array($this, 'executeWithContext'), $args);
  }

  /**
   * Executes the specified operation.
   *
   * @param string $operation
   *   The operation name. If a configuration item can be found, its value will
   *   be returned. Otherwise, if a callable is found matching the operation it
   *   will be called and its result will be returned. Any additional parameter
   *   will be forwarded to the callable.
   * @param array $context
   *   A specific context to be merged with the default one. Axis values
   *   specified here take precedence over the default ones.
   *
   * @return mixed
   *   The operation result.
   *
   * @throws \LogicException
   *   When no matching item can be found.
   */
  public function executeWithContext($operation, array $context) {
    $candidate = $this->getMatchingExecutionCandidate($operation, $context);

    if (is_callable($candidate)) {
      $args = array_slice(func_get_args(), 2);
      $args[] = $this->getMergedContext($context);
      $result = call_user_func_array($candidate, $args);
    }
    else {
      $result = $candidate;
    }

    return $result;
  }

  /**
   * Looks for the execution candidate fitting the current context best.
   *
   * @param string $operation
   *   The operation name.
   * @param $context
   *   The current context.
   *
   * @return mixed
   *   A callable or a configured return value.
   */
  protected function getMatchingExecutionCandidate($operation, $context) {
    $matching_candidate = NULL;
    $found = FALSE;
    $search = array('&', '=');
    $replace = array('__', '_');
    $candidates = $this->getCandidates($context, $search[0], $search[1]);
    $configuration = $this->configuration;

    // If the specified operation is actually a function name, we add it to the
    // candidate list as fall back item.
    if (function_exists($operation)) {
      $candidates[] = $operation;
      $addition = array($operation => $operation);
      if (!isset($configuration[$operation])) {
        $configuration[$operation] = $addition;
      }
      else {
        $configuration[$operation] += $addition;
      }
    }

    foreach ($candidates as $candidate) {
      // If we have a valid configuration item we just return it.
      if (isset($configuration[$operation][$candidate])) {
        $this->lastContextKey = $candidate;
        $item = $configuration[$operation][$candidate];
        // In the configuration we can specify a "shortcut" value to indicate
        // the operation name as callback.
        if ($item == static::BASE_REFERENCE) {
          $item = $operation;
        }
        $matching_candidate = $item;
        $found = TRUE;
        break;
      }
      else {
        // Convert the candidate into a function name and check whether it is
        // defined, in which case we return it.
        $callback = $operation . '__'  . str_replace($search, $replace, $candidate);
        if (is_callable($callback)) {
          $this->lastContextKey = $candidate;
          $matching_candidate = $callback;
          $found = TRUE;
          break;
        }
      }
    }

    if (!$found) {
      throw new \LogicException('No matching candidate found.');
    }

    return $matching_candidate;
  }

  /**
   * Retrieves the specified variable value.
   *
   * @param string $name
   *   The variable name. If a configuration item can be found, its value will
   *   be returned. Otherwise, the value of a variable with the specified name
   *   will be returned.
   * @param mixed $default
   *   (optional) The variable default value. Defaults to NULL.
   * @param array $context
   *   (optional) A specific context to be merged with the default one. Axis
   *   values specified here take precedence over the default ones. Defaults to
   *   none.
   *
   * @return mixed
   *   The variable value.
   */
  public function getVariable($name, $default = NULL, $context = array()) {
    $value = $this->getMatchingVariableCandidate($name, $context);

    if (!isset($value)) {
      $value = variable_get($name, $default);
    }

    return $value;
  }

  /**
   * Looks for the variable candidate fitting the current context best.
   *
   * @param string $operation
   *   The variable name.
   * @param $context
   *   The current context.
   *
   * @return mixed
   *   A variable or a configured value.
   */
  protected function getMatchingVariableCandidate($operation, $context) {
    $matching_candidate = NULL;
    $configuration = $this->configuration;
    $candidates = $this->getCandidates($context, '&', '=');

    foreach ($candidates as $candidate) {
      if (isset($configuration[$operation][$candidate])) {
        $this->lastContextKey = $candidate;
        $item = $configuration[$operation][$candidate];
        // In the configuration we can specify a "shortcut" value to indicate
        // the operation name as callback.
        if ($item == static::BASE_REFERENCE) {
          $item = $operation;
        }
        $matching_candidate = $item;
        break;
      }
    }

    return $matching_candidate;
  }

  /**
   * Preprocesses variables for a theme template.
   *
   * This will populate theme hook suggestions with the candidates matching the
   * current context.
   *
   * @param array $variables
   *   The theme template variables.
   * @param array $context
   *   (optional) A specific context to be merged with the default one. Axis
   *   values specified here take precedence over the default ones. Defaults to
   *   none.
   */
  public function preprocessTemplate(array &$variables, $context = array()) {
    $separator = '__';
    $candidates = $this->getCandidates($context, $separator, '_');
    unset($candidates['*']);
    $registry = theme_get_registry(FALSE);
    $hook = $variables['theme_hook_original'];

    if (isset($registry[$hook])) {
      $info = $registry[$hook];
      if (isset($info['pattern'])) {
        $hook = $info['pattern'];
      }
      elseif (isset($info['template'])) {
        $hook = $info['template'] . $separator;
      }
      else {
        $hook .= $separator;
      }
    }

    foreach (array_reverse($candidates) as $candidate) {
      $variables['theme_hook_suggestions'][] = $hook . $candidate;
    }
    $variables['branch_context'] = $this->getMergedContext($context);
  }

  /**
   * Computes the list of candidates for the current context.
   *
   * Candidates will assume the following form:
   *
   * axis_1=value_1&axis_2=value_2&axis_3=value_3
   *
   * @param $context
   *   A specific context to be merged with the default one. Axis values
   *   specified here take precedence over the default ones.
   * @param $axis_separator
   *   The separator between axes.
   * @param $separator
   *   The separator between axis name and value.
   *
   * @return string[]
   *   An array of candidates.
   */
  protected function getCandidates($context, $axis_separator, $separator) {
    $candidates = array();
    $context = $this->getMergedContext($context);
    $combinations = $this->getCombinations(array_keys($context));

    $weight = array_flip(array_keys($context));
    $this->sortCombinations($combinations, $weight);

    foreach ($combinations as $combination) {
      $candidate = '';
      foreach ($combination as $axis) {
        $value = is_bool($context[$axis]) ? (int) $context[$axis] : $context[$axis];
        $candidate .= ($candidate ? $axis_separator : '') . $axis . $separator . $value;
      }
      if ($candidate) {
        $candidates[] = $candidate;
      }
    }

    // Always add the fallback candidate.
    $candidates['*'] = '*';

    return $candidates;
  }

  /**
   * Sorts the axis combinations according to the configured precedence.
   *
   * Combinations having a higher number of axes will take precedence.
   * Combinations having the same number of axes will be internally sorted by
   * applying the configured axis weights:
   *
   * axis_1=value_1&axis_2=value_2&axis_3=value_3
   * axis_2=value_2&axis_3=value_3
   * axis_1=value_1&axis_3=value_3
   * axis_1=value_1&axis_2=value_2
   * axis_3=value_3
   * axis_2=value_2
   * axis_1=value_1
   *
   * @param array $combinations
   *   A reference to an array of axis combinations to be sorted.
   * @param array $weight
   *   An array of axis weights keyed by axis name.
   */
  protected function sortCombinations(array &$combinations, array $weight) {
    foreach ($combinations as &$combination) {
      usort($combination, function($a, $b) use ($weight) {
        return $weight[$a] - $weight[$b];
      });
    }

    usort($combinations, function($a, $b) use ($weight) {
      $result = count($b) - count($a);

      if (!$result) {
        $i = 0;
        while ($a[$i] == $b[$i]) ++$i;
        $result = $weight[$a[$i]] - $weight[$b[$i]];
      }

      return $result;
    });
  }

  /**
   * Merges the specified context with the default one.
   *
   * @param $context
   *   A specific context to be merged with the default one. Axis values
   *   specified here take precedence over the default ones.
   *
   * @return array
   *   The merged context.
   */
  protected function getMergedContext($context) {
    $merged_context = array();
    $axes = array_keys($this->context + $context);
    foreach ($axes as $axis) {
      $merged_context[$axis] = isset($context[$axis]) ? $context[$axis] : $this->context[$axis];
    }
    return $merged_context;
  }

  /**
   * Computes all the possible combinations for the given values.
   *
   * @param array $values
   *   An array of values to be combined.
   *
   * @return mixed[][]
   *   An array of arrays of values.
   */
  protected function getCombinations(array $values) {
    $combinations = array();

    // See https://r.je/php-find-every-combination.html
    $num = count($values);
    $total = pow(2, $num);

    for ($i = 0; $i < $total; $i++) {
      $current = [];
      for ($j = 0; $j < $num; $j++) {
        if (pow(2, $j) & $i) {
          $current[] = $values[$j];
        }
      }
      $combinations[] = $current;
    }

    return $combinations;
  }

}
